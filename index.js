const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
// Import Routes
const authRoutes = require('./routes/auth');

dotenv.config();

// connect to db
mongoose.connect(process.env.DB_CONNECT,
    {useNewUrlParser: true, useUnifiedTopology: true},
    () => console.log("Connected to Databse"));

app.listen(3000, () => console.log('Server is up and running '));

//Middlewares
app.use(express.json());

// Route MiddleWares
app.use('/api', authRoutes);
