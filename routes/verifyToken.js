const jwt = require('jsonwebtoken');

module.exports = function(req,res,next) {
    const Token = req.header('auth-token');
    if(!Token) return res.status(401).send('access denied');

    try{
        const verified = jwt.verify(Token,process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    }
    catch(err){
        res.status(400).send('Invalid token');
    }
}