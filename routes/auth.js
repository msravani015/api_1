const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const joi = require('joi');
const verify = require('./verifyToken');
const something = require('../model/User');


const regschema = joi.object({
    name : joi.string()
           .alphanum()
           .min(6)
           .max(20)
           .required(),
   email : joi.string()
           .min(4)
           .max(20)
           .email(),
    password : joi.string()
             .min(5)
             .max(10)
             .required(),
})



const login = joi.object({
  
   email : joi.string()
           .min(4)
           .max(20)
           .required(),
    password : joi.string()
             .min(5)
             .max(10)
             .required(),
})

router.post('/register', async (req,res) => {

    const { error } = regschema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

// console.log(req.body.email);


const emailExist = await something.findOne({ email: req.body.email });
if (emailExist) return res.status(400).send('Email already exists');

const salt = await bcrypt.genSalt(10);
const hashpassword = await bcrypt.hash(req.body.password, salt);

console.log(hashpassword);

    // creating a new user.. using mongoose schema..
    // preparation for data acoording the schema rules and data to be submitted.
const user = new something({
    name: req.body.name,
    email: req.body.email,
    password: hashpassword
});
try {
    // submit the data into database  (prepareddata.save())
      const savedUser = await user.save();
    res.send({id: savedUser._id, savedName: savedUser.name});
}catch(err) {
    console.log(err);
    res.status(400).send("error");
    // console.log(err)
}
});


router.post('/login', async (req,res) => {

    const { error } = login.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

  //email check while login
    const emailExist = await something.findOne({ email: req.body.email }); //pull the data id,name,email,password
if (!emailExist) return res.status(400).send('Email doesnt exists');

//password check for login
const ValidPass = await bcrypt.compare(req.body.password,emailExist.password);
if(!ValidPass) return res.status(400).send("There something wrong with the username and password");

//create token
const Token = jwt.sign({_id: emailExist._id},process.env.TOKEN_SECRET);
res.header('auth-token',Token).send(Token); //saving the token in server
});


module.exports = router;


router.get('/get-data',verify, async (req,res) => {
    res.send("yo reached the get function");
})

module.exports = router;